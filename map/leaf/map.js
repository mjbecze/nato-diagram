function init_map(){
    
    var data_location = "http://natoprotest.org/media/diagram/map/data/";
    var chicago = new L.LatLng(41.882364,-87.628055);
    var tilesetUrl =  'http://{s}.acetate.geoiq.com/tiles/acetate/{z}/{x}/{y}.png';
    var tilesetAttrib = '2011 GeoIQ &#038; Stamen, Data from OSM and Natural Earth';

    // create the tile layer with correct attribution
    var osm = new L.TileLayer(tilesetUrl, {
        minZoom: 2,
        maxZoom: 20,
        attribution: tilesetAttrib
    }); 

    // set up the map
    var map = new L.Map('map',{
        center:chicago,
        zoom: 15,
        layers: [osm],
        zoomControl:false
    });

    //obama's HQ
    var altIcon  = L.Icon.extend({
        iconUrl: data_location+'Icons/marker-alt.png',

    });

      
    //setup Icons
    var ImageIcon = L.Icon.extend({
        iconUrl: data_location+'Icons/police.png',
        shadowUrl: null,
        iconSize: new L.Point(70, 70),
        iconAnchor: new L.Point(35, 70),
        popupAnchor: new L.Point(0, -45)
    });

    //add donors
    var DonorsLayer = new L.GeoJSON(null, {
        pointToLayer: function (latlng) {
                return new L.Marker(latlng, {icon: new ImageIcon()});
                //return new L.Circle(latlng, 40, StationMarkerOptions);
        }            
    });
    
    DonorsLayer.on("featureparse", function (e) {
        e.layer.options.icon.iconUrl = gen_icon(e.properties.industry);
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2>"+ e.properties.address+"<br>"+e.properties.description);
        }
    });  
    
    $.ajax({
       url: data_location+"donors.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        DonorsLayer.addGeoJSON(data);
    });
    map.addLayer(DonorsLayer);
    
    //add World Bussness
    var WorldbussnessChiLayer = new L.GeoJSON(null, {
         pointToLayer: function (latlng) {
            return new L.Marker(latlng, {icon: new ImageIcon()});
        }             
    });   
    
    WorldbussnessChiLayer.on("featureparse", function (e) {
        e.layer.options.icon.iconUrl = gen_icon(e.properties.industry);
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2>"+e.properties.address+"<br>"+e.properties.description);
        }
    });     

    $.ajax({
       url: data_location+"worldbussnessChi.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        WorldbussnessChiLayer.addGeoJSON(data);
    });
    
    /*
     * Banks
     */ 
     
    //Chase
    
    //marker settings
    var ChaseMarkerOptions = {
        fillColor: "#ff7800",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };
        
    //set up geojson layer
    var ChaseBankLayer = new L.LayerGroup();
    var ChaseBankJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    }); 
    var ChaseHQ = new L.Marker(new L.LatLng(41.881793,-87.63008)).bindPopup("<b>Headquarters:</b> Chase Tower, 10 South Dearborn Street");
    ChaseBankLayer.addLayer(ChaseHQ).addLayer(ChaseBankJSON);
    //set up popups
    ChaseBankJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Chase ATM or Branch</h1>"+e.properties.Description);
        }
        
    });  
    //Chase bank data
    $.ajax({
       url: data_location+"chase.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        ChaseBankJSON.addGeoJSON(data);
    });
   
   //BOA
    var BoALayer =  new L.LayerGroup();
    var BoAJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    });   
    var BoAHQ = new L.Marker(new L.LatLng(41.879908,-87.632161)).bindPopup("<b>Headquarters:</b> Bank of America Building, 135 South LaSalle ");
    BoALayer.addLayer(BoAJSON).addLayer(BoAHQ);
    
    BoAJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Bank Of A. ATM or Branch</h1>"+e.properties.Description);
        }
    });  
    
    $.ajax({
       url: data_location+"BoA.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        BoAJSON.addGeoJSON(data);
    });
    
    //citi   
    var CitiLayer =  new L.LayerGroup();
    var CitiJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    });   
    
    var CitiHQ = new L.Marker(new L.LatLng(41.882138,-87.640405)).bindPopup("<b>Headquarters:</b> Citigroup Center, 500 West Madison");
    CitiLayer.addLayer(CitiJSON).addLayer(CitiHQ);    
    CitiJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Citi ATM or Branch</h1>"+e.properties.Description+"<a>");
        }
    });  
     
    $.ajax({
       url: data_location+"citi.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        CitiJSON.addGeoJSON(data);
    });
    
    //police stations
    //create custom Icons
    var policeIcon = new ImageIcon(); 
    var StationLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Marker(latlng, {icon: policeIcon});
                //return new L.Circle(latlng, 40, StationMarkerOptions);
        }            
    }); 
    StationLayer.on("featureparse", function (e) {
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<b>Station:"+e.properties.name+"</b><br>"+e.properties.address+"<br><a href=\""+e.properties.description+"\">More Info</a>");
        }
    }); 
     
    $.ajax({
       url: data_location+"stations.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        StationLayer.addGeoJSON(data);
    });

    //Hospitals
    
    var HospitalMarkerOptions = {
        fillColor: "White",
        color: "Red",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };   
    var HospitalLayer = new L.GeoJSON(null, {
        pointToLayer: function (latlng) {
            return new L.Circle(latlng, 40, HospitalMarkerOptions);
        }               
    }); 

    HospitalLayer.on("featureparse", function (e) {
        if (e.properties){
            e.layer.bindPopup("<b>"+e.properties.name+"</b>");
        }
    });  
    
    $.ajax({
       url: data_location+"hospitals.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        HospitalLayer.addGeoJSON(data);
    });    
        
    //CCTV
        
    var CCTVMarkerOptions = {
        fillColor: "black",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };

    var CCTVLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, CCTVMarkerOptions);
        }            
    });  
    
    CCTVLayer.on("featureparse", function (e) {
        if (e.properties ){
            e.layer.bindPopup(e.properties.Description);
        }
    }); 
    //CCTV  data
    $.ajax({
       url: data_location+"cctv.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        CCTVLayer.addGeoJSON(data);
    });
     
    //Homes

    var HouseMarkerOptions = {
        fillColor: "#20B727",
        color: "#5454EF",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };
    
    var HouseLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, HouseMarkerOptions);
        }            
    }); 
    
    HouseLayer.on("featureparse", function (e) {
        if (e.properties){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2><b>Entry:</b>"+e.properties.description+"<br>"+e.properties.address);
        }
    });    
    
    $.ajax({
       url: data_location+"build.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        HouseLayer.addGeoJSON(data);
    });

    //setup controlls
    var layersControl = new L.Control.Layers(null, {
            "Summit Sponsors": DonorsLayer,
            "World Bussness Chicago":WorldbussnessChiLayer,
            "Chase Banks": ChaseBankLayer,
            "Bank Of America":BoALayer,
            "Citi": CitiLayer,
            "Police Stations":StationLayer,
            "Hospitals":HospitalLayer,
            "Close Circit Cameras": CCTVLayer,
            "Abandon Homes": HouseLayer

        },{'collapsed':false});

    map.addControl(layersControl);
    
    function gen_icon(industry){
        if(industry == "food"){
            return data_location+'Icons/agribusiness.png';
        }else if(industry == "retail"){
            return data_location+'Icons/retail.png';
        }else if(industry == "legal"){
            return data_location+'Icons/law.png';
        }else if(industry == "logistics"){
            return data_location+'Icons/shipping.png';
        }else if(industry == "nonprofit"){
            return data_location+'Icons/non-profit-org.png';
        }else if(industry == "realestate"){
            return data_location+'Icons/real-estate.png';
        }else if(industry == "energy"){
            return data_location+'Icons/energy.png';
        }else if(industry == "petrol"){
            return data_location+'Icons/petroleum.png';
        }else if(industry == "coordinating"){
            return data_location+'Icons/coordinating-committees.png';
        }else if(industry == "cultural"){
            return data_location+'Icons/culture.png';
        }else if(industry == "financial"){
            return data_location+'Icons/finance.png';
        }else if(industry == "aerospace"){
            return data_location+'Icons/aerospace-and-defense.png';
        }else if(industry == "pharmaceuticals"){
            return data_location+'Icons/pharma.png';
        }else if(industry == "health"){
            return data_location+'Icons/pharma.png';     
        }else if(industry == "insurance"){ 
            return data_location+'Icons/real-estate.png';
        }else if(industry == "university"){
            return data_location+'Icons/university.png';
        }else if(industry == "cultural"){ 
            return data_location+'Icons/culture.png';
        }else if(industry == "technology"){ 
            return data_location+'Icons/technology.png';
        }else if(industry == "telecom"){ 
            return data_location+'Icons/telecom.png';
        }else if(industry == "media"){ 
            return data_location+'Icons/media.png';
        }else if(industry == "manufacturing"){ 
            return data_location+'Icons/industrial-manufacturing.png';
        }else{
            return data_location+'Icons/Chemical.png';
        }
    }
    
    $($(".leaflet-control-layers-overlays").children()[1]).after('<hr>');
    $($(".leaflet-control-layers-overlays").children()[5]).after('<hr>');
}
