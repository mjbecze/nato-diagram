function init(){
    $("#svg").svg({loadURL: 'http://natoprotest.org/media/diagram/Diagram-copy.svg',onLoad: loadDone});
    var svg = $('#svg').svg('get')
    //$(".grouped",svg.root()).hover(function(){alert("test");})
    function loadDone(){
        $('#diagram g', svg.root()).not(".text"). 
                bind('mouseover', svgOver).bind('mouseout', svgOut);
        function svgOver() { 
            $(this).attr('stroke', 'lime').attr( 'fill', 'blue'); 
        } 
         
        function svgOut() { 
            $(this).attr('stroke', 'black').attr( 'fill', 'white'); 
        }
    }
}
